package main

import (
	"log"
	"math/rand"
	"time"

	game "gitlab.com/morzack/2hu-2020/game"

	"github.com/hajimehoshi/ebiten"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	game, err := game.NewGameInstance()
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}
