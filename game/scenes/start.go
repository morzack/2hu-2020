package scenes

import "gitlab.com/morzack/2hu-2020/game/engine"

func NewStartScene(sceneManager engine.SceneManagerInterface) (engine.SceneInterface, error) {
	startBg, err := engine.NewBackground("./assets/backgrounds/startbg.png")
	if err != nil {
		return nil, err
	}
	startScene, err := engine.NewScene(sceneManager, "start", startBg)
	if err != nil {
		return nil, err
	}

	// populate with buttons
	startButton, err := engine.NewButtonStaticSprites(startScene, 50, 200, 200, 75, "start", "./assets/buttons/start")
	if err != nil {
		return nil, err
	}
	startButton.RegisterOnClick(func() error {
		return startScene.Transition("levelPlayer")
	})
	startScene.RegisterActor("startButton", &startButton)

	return startScene, nil
}
