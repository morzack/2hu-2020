package scenes

import (
	"gitlab.com/morzack/2hu-2020/game/engine"
)

func NewLevelScene(sceneManager engine.SceneManagerInterface) (engine.SceneInterface, error) {
	bg, err := engine.NewBackground("./assets/backgrounds/levelbg.png")
	if err != nil {
		return nil, err
	}
	levelScene, err := engine.NewScene(sceneManager, "levelPlayer", bg)
	if err != nil {
		return nil, err
	}

	// populate UI
	// endButton, err := engine.NewButtonStaticSprites(levelScene, 500, 50, 200, 75, "endButton", "./assets/buttons/start")
	// if err != nil {
	// 	return nil, err
	// }
	// endButton.RegisterOnClick(func() error {
	// 	return levelScene.Transition(engine.StopSceneId)
	// })
	// levelScene.RegisterActor("endButton", &endButton)

	score, err := engine.NewScore(levelScene, 15, 195, true)
	if err != nil {
		return nil, err
	}
	levelScene.RegisterActor(engine.ScoreActorId, score)

	levelTimer, err := engine.NewLevelTimer(levelScene, 15, 240, 60*7) // PEKO this is game duration
	if err != nil {
		return nil, err
	}
	levelScene.RegisterActor(engine.LevelTimerActorId, levelTimer)

	// create map
	tileMap, err := engine.NewTileMap(levelScene, "./data/maps/testmap.txt", 12, 10, 50, 50)
	if err != nil {
		return nil, err
	}
	levelScene.RegisterActor(engine.TileMapActorId, &tileMap)

	// make player
	player, err := engine.NewPlayer(levelScene, tileMap.GetPlayerStartPos())
	if err != nil {
		return nil, err
	}
	levelScene.RegisterActor(engine.PlayerActorId, &player)

	// add orders
	orderQueue, err := engine.NewOrderQueue(levelScene, 10, 10)
	if err != nil {
		return nil, err
	}
	orderQueue.StartServing()
	levelScene.RegisterActor(engine.OrderQueueActorId, orderQueue)

	return levelScene, nil
}
