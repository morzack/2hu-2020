package scenes

import (
	"image/color"

	"gitlab.com/morzack/2hu-2020/game/engine"
)

func NewScoreScene(sceneManager engine.SceneManagerInterface) (engine.SceneInterface, error) {
	bg, err := engine.NewBlankBackground(color.Black)
	if err != nil {
		return nil, err
	}
	scoreScene, err := engine.NewScene(sceneManager, "levelScore", bg)
	if err != nil {
		return nil, err
	}

	// populate UI
	returnToMenuButton, err := engine.NewButtonStaticSprites(scoreScene, 300-200/2, 250-75/2, 200, 75, "returnButton", "./assets/buttons/menu")
	if err != nil {
		return nil, err
	}
	returnToMenuButton.RegisterOnClick(func() error {
		return scoreScene.Transition("levelPlayer")
	})
	scoreScene.RegisterActor(returnToMenuButton.GetName(), &returnToMenuButton)

	score, err := engine.NewScore(scoreScene, 300-200/2+50, 250-75/2-80, false)
	if err != nil {
		return nil, err
	}
	scoreScene.RegisterActor(score.GetName(), score)

	return scoreScene, nil
}
