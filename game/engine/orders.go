package engine

import (
	"fmt"
	"math/rand"

	"github.com/hajimehoshi/ebiten"
)

const (
	// NOTE this is where most difficulty can be changed
	// peko
	OrderFrequencyStart = 15
	OrderFrequencyDrop  = .25
	OrderFrequencyMin   = 10

	OrderUIBufferWidth = 20 // px between orders
)

type OrderQueueInterface interface {
	ActorInterface

	PushNewOrder(orderType OrderType) error
	PushNewRandomOrder() error
	CompleteOrder(orderType OrderType) (bool, error) // bool determines if order was actually consumed
	GetOrders() []OrderInterface
	FailOrder(orderActorName string) (bool, error)

	StartServing()
	StopServing()
}

type OrderQueue struct {
	Actor

	orders        []OrderInterface
	newOrderTimer *Timer

	orderFrequency float64
}

func NewOrderQueue(scene SceneInterface, x, y float64) (OrderQueueInterface, error) {
	orderQueue := OrderQueue{
		Actor{
			x:         x,
			y:         y,
			actorType: ActorTypeOrderQueue,
			name:      GetID("orderQueueActor"),
			scene:     scene,
		},
		make([]OrderInterface, 0),
		&Timer{},
		OrderFrequencyStart,
	}

	orderTimer := NewTimer(scene, GetID("newOrderTimer"))
	orderTimer.SetTargetSeconds(orderQueue.orderFrequency)
	orderTimer.SetListener(func() error {
		// spawn a new order at random and make more orders appear faster
		if err := orderQueue.PushNewRandomOrder(); err != nil {
			return err
		}
		orderQueue.orderFrequency -= OrderFrequencyDrop
		if orderQueue.orderFrequency <= OrderFrequencyMin {
			orderQueue.orderFrequency = OrderFrequencyMin
		}
		orderTimer.SetTargetSeconds(orderQueue.orderFrequency)
		orderTimer.Start()
		return nil
	})
	orderTimer.Reset()
	orderTimer.Randomize()
	scene.RegisterActor(orderTimer.Actor.GetName(), &orderTimer)
	orderQueue.newOrderTimer = &orderTimer

	return &orderQueue, nil
}

func (o *OrderQueue) PushNewOrder(orderType OrderType) error {
	var newOrder OrderInterface
	var err error
	switch orderType {
	case OrderTypeSoba:
		newOrder, err = NewSobaOrder(o.scene, o)
	case OrderTypeTakoyaki:
		newOrder, err = NewTakoyakiOrder(o.scene, o)
	case OrderTypeYakitori:
		newOrder, err = NewYakitoriOrder(o.scene, o)
	}
	if err != nil {
		return err
	}
	o.Actor.scene.RegisterActor(newOrder.GetName(), newOrder)
	o.orders = append(o.orders, newOrder)
	return nil
}

func (o *OrderQueue) PushNewRandomOrder() error {
	orderType := rand.Intn(3)
	return o.PushNewOrder(OrderType(orderType))
}

func (o *OrderQueue) CompleteOrder(orderType OrderType) (bool, error) {
	var deletingOrder OrderInterface
	deletingOrder = nil
	for i, order := range o.orders {
		if order.GetOrderType() == orderType {
			deletingOrder = order
			o.orders = append(o.orders[:i], o.orders[i+1:]...)
			break
		}
	}
	if deletingOrder == nil {
		return false, nil
	}
	o.Actor.scene.DeleteActor(deletingOrder.GetName())
	return true, nil
}

func (o *OrderQueue) GetOrders() []OrderInterface {
	return o.orders
}

func (o *OrderQueue) FailOrder(orderActorName string) (bool, error) {
	for i, order := range o.orders {
		if order.GetName() == orderActorName {
			o.Actor.scene.DeleteActor(order.GetName())
			o.orders[i] = nil
			o.orders = append(o.orders[:i], o.orders[i+1:]...)
			return true, nil
		}
	}
	return false, nil
}

func (o *OrderQueue) StartServing() {
	o.newOrderTimer.Start()
}

func (o *OrderQueue) StopServing() {
	o.newOrderTimer.Stop()
}

func (o OrderQueue) Draw(screen *ebiten.Image) error {
	totalTraversed := 0.0 // px
	for _, order := range o.orders {
		oSprite := order.GetSprite()
		w, h := oSprite.GetSize()
		if err := oSprite.Render(screen, o.x+totalTraversed+OrderUIBufferWidth, o.y, w, h, 0); err != nil {
			return err
		}
		if err := order.DrawProgressBar(screen, o.x+totalTraversed+OrderUIBufferWidth+10, o.y+h-25, w-20, 25); err != nil {
			return err
		}
		totalTraversed += w + OrderUIBufferWidth
	}

	return nil
}

type OrderType int

const (
	OrderTypeTakoyaki OrderType = 0
	OrderTypeYakitori OrderType = 1
	OrderTypeSoba     OrderType = 2

	TakoyakiTimer = 25
	YakitoriTimer = 25
	SobaTimer     = 25
)

type OrderInterface interface {
	ActorInterface

	GetOrderType() OrderType
	GetSprite() SpriteInterface

	DrawProgressBar(screen *ebiten.Image, x, y, w, h float64) error
}

type Order struct {
	Actor

	orderSprite SpriteInterface

	orderQueue       OrderQueueInterface
	orderType        OrderType
	orderTimer       *Timer
	orderProgressBar ProgressBarInterface
}

func NewOrder(scene SceneInterface, orderQueue OrderQueueInterface, sprite SpriteInterface, orderType OrderType, orderTime float64) (OrderInterface, error) {
	order := Order{
		Actor{
			actorType: ActorTypeOrder,
			name:      GetID(fmt.Sprintf("order%dActor", orderType)),
			scene:     scene,
		},
		sprite,
		orderQueue,
		orderType,
		&Timer{},
		nil,
	}

	orderTimer := NewTimer(scene, GetID("orderTimeoutTimer"))
	orderTimer.SetTargetSeconds(orderTime)
	orderTimer.SetListener(func() error {
		if _, err := order.orderQueue.FailOrder(order.name); err != nil {
			return err
		}
		return nil
	})
	orderTimer.Reset()
	orderTimer.Start()
	scene.RegisterActor(orderTimer.GetName(), &orderTimer)
	order.orderTimer = &orderTimer

	progressBar, err := NewProgressBar(scene, func() float64 {
		if order.orderTimer.active {
			return order.orderTimer.GetPercentComplete()
		}
		return 0
	}, "./assets/progress-bar", 0, 0, 150-20, 25)
	if err != nil {
		return nil, err
	}
	order.orderProgressBar = progressBar

	return &order, nil
}

func NewTakoyakiOrder(scene SceneInterface, orderQueue OrderQueueInterface) (OrderInterface, error) {
	takoyakiOrderSprite, err := NewSpriteFromPath("./assets/orders/takoyaki.png")
	if err != nil {
		return nil, err
	}
	return NewOrder(scene, orderQueue, takoyakiOrderSprite, OrderTypeTakoyaki, TakoyakiTimer)
}

func NewYakitoriOrder(scene SceneInterface, orderQueue OrderQueueInterface) (OrderInterface, error) {
	yakitoriOrderSprite, err := NewSpriteFromPath("./assets/orders/yakitori.png")
	if err != nil {
		return nil, err
	}
	return NewOrder(scene, orderQueue, yakitoriOrderSprite, OrderTypeYakitori, YakitoriTimer)
}

func NewSobaOrder(scene SceneInterface, orderQueue OrderQueueInterface) (OrderInterface, error) {
	sobaOrderSprite, err := NewSpriteFromPath("./assets/orders/soba.png")
	if err != nil {
		return nil, err
	}
	return NewOrder(scene, orderQueue, sobaOrderSprite, OrderTypeSoba, SobaTimer)
}

func (o Order) GetOrderType() OrderType {
	return o.orderType
}

func (o Order) GetSprite() SpriteInterface {
	return o.orderSprite
}

func (o Order) DrawProgressBar(screen *ebiten.Image, x, y, w, h float64) error {
	return o.orderProgressBar.GetSprite().Render(screen, x, y, w, h, 0)
}
