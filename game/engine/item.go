package engine

import "github.com/hajimehoshi/ebiten"

type ItemType int

type ItemDispenseFunction func(scene SceneInterface) (ItemInterface, error)

const (
	ItemTypePlaceholder ItemType = iota

	ItemTypeOvercooked ItemType = iota
	ItemTypePlate      ItemType = iota
	ItemTypeSkewer     ItemType = iota

	ItemTypeOctopus          ItemType = iota
	ItemTypeBatter           ItemType = iota
	ItemTypeTakoyaki         ItemType = iota
	ItemTypePreparedTakoyaki ItemType = iota

	ItemTypeChicken          ItemType = iota
	ItemTypeYakitori         ItemType = iota
	ItemTypePreparedYakitori ItemType = iota

	ItemTypeRawSoba      ItemType = iota
	ItemTypeSoba         ItemType = iota
	ItemTypePreparedSoba ItemType = iota

	TinyItemWidth   = 30
	TinyItemHeight  = 30
	SmallItemWidth  = 45
	SmallItemHeight = 45
)

type ItemInterface interface {
	ActorInterface
	GetBoundingBox() Rect
	GetItemType() ItemType
	SetSize(w, h float64)
}

type Item struct {
	Actor
	width, height float64
	itemType      ItemType
	sprite        SpriteInterface
}

func (i Item) Draw(screen *ebiten.Image) error {
	return i.sprite.Render(screen, i.x, i.y, i.width, i.height, 0)
}

func (i Item) GetItemType() ItemType {
	return i.itemType
}

func (i Item) GetBoundingBox() Rect {
	return Rect{
		p1: Point{i.x, i.y},
		p2: Point{i.width, i.height},
	}
}

func (i *Item) SetSize(w, h float64) {
	i.width = w
	i.height = h
}

func NewPlaceholderItem(scene SceneInterface) (ItemInterface, error) {
	placeholderSprite, err := NewSpriteFromPath("./assets/items/placeholder.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("placeholderItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypePlaceholder,
		placeholderSprite,
	}, nil
}

func NewOvercookedItem(scene SceneInterface) (ItemInterface, error) {
	overcookedSprite, err := NewAnimatedSprite("./assets/items/overcooked", 46, 4)
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("overcookedItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeOvercooked,
		overcookedSprite,
	}, nil
}

func NewPlateItem(scene SceneInterface) (ItemInterface, error) {
	plateSprite, err := NewSpriteFromPath("./assets/items/plate.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("plateItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypePlate,
		plateSprite,
	}, nil
}

func NewSkewerItem(scene SceneInterface) (ItemInterface, error) {
	skewerSprite, err := NewSpriteFromPath("./assets/items/skewer.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("skewerItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeSkewer,
		skewerSprite,
	}, nil
}

func NewOctopusItem(scene SceneInterface) (ItemInterface, error) {
	octopusSprite, err := NewSpriteFromPath("./assets/items/octopus.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("octopusItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeOctopus,
		octopusSprite,
	}, nil
}

func NewBatterItem(scene SceneInterface) (ItemInterface, error) {
	batterSprite, err := NewSpriteFromPath("./assets/items/batter.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("batterItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeBatter,
		batterSprite,
	}, nil
}

func NewTakoyakiItem(scene SceneInterface) (ItemInterface, error) {
	takoyakiSprite, err := NewSpriteFromPath("./assets/items/takoyaki.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("takoyakiItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeTakoyaki,
		takoyakiSprite,
	}, nil
}

func NewPreparedTakoyakiItem(scene SceneInterface) (ItemInterface, error) {
	preparedTakoyakiSprite, err := NewSpriteFromPath("./assets/items/takoyaki-prepared.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("preparedTakoyakiItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypePreparedTakoyaki,
		preparedTakoyakiSprite,
	}, nil
}

func NewChickenItem(scene SceneInterface) (ItemInterface, error) {
	chickenSprite, err := NewSpriteFromPath("./assets/items/chicken.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("chickenItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeChicken,
		chickenSprite,
	}, nil
}

func NewYakitoriItem(scene SceneInterface) (ItemInterface, error) {
	yakitoriSprite, err := NewSpriteFromPath("./assets/items/yakitori.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("yakitoriItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeYakitori,
		yakitoriSprite,
	}, nil
}

func NewPreparedYakitoriItem(scene SceneInterface) (ItemInterface, error) {
	preparedYakitoriSprite, err := NewSpriteFromPath("./assets/items/yakitori-prepared.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("preparedYakitoriItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypePreparedYakitori,
		preparedYakitoriSprite,
	}, nil
}

func NewRawSobaItem(scene SceneInterface) (ItemInterface, error) {
	rawSobaSprite, err := NewSpriteFromPath("./assets/items/raw-soba.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("rawSobaItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeRawSoba,
		rawSobaSprite,
	}, nil
}

func NewSobaItem(scene SceneInterface) (ItemInterface, error) {
	sobaSprite, err := NewSpriteFromPath("./assets/items/soba.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("sobaItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypeSoba,
		sobaSprite,
	}, nil
}

func NewPreparedSobaItem(scene SceneInterface) (ItemInterface, error) {
	preparedSobaSprite, err := NewSpriteFromPath("./assets/items/soba-prepared.png")
	if err != nil {
		return nil, err
	}
	return &Item{
		Actor{
			name:      GetID("preparedSobaItem"),
			scene:     scene,
			actorType: ActorTypeItem,
		},
		SmallItemWidth, SmallItemHeight,
		ItemTypePreparedSoba,
		preparedSobaSprite,
	}, nil
}
