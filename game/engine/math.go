package engine

import "math"

type Point struct {
	x, y float64
}

func (p1 Point) GetDistance(p2 Point) float64 {
	return math.Hypot((p2.x - p1.x), (p2.y - p1.y))
}

type Rect struct {
	p1, p2 Point
}

func (r1 Rect) CollidesWithRect(r2 Rect) bool {
	if r1.p1.x < r2.p1.x+r2.p2.x &&
		r1.p1.x+r1.p2.x > r2.p1.x &&
		r1.p1.y < r2.p1.y+r2.p2.y &&
		r1.p1.y+r1.p2.y > r2.p1.y {
		return true
	}
	return false
}

func (r Rect) CollidesWithPoint(p Point) bool {
	if p.x > r.p1.x && p.x < r.p1.x+r.p2.x &&
		p.y > r.p1.y && p.y < r.p1.y+r.p2.y {
		return true
	}
	return false
}

func (r Rect) GetCenterPoint() Point {
	return Point{
		x: (2*r.p1.x + r.p2.x) / 2,
		y: (2*r.p1.y + r.p2.y) / 2,
	}
}

func (r Rect) GetCenterDistance(p Point) float64 {
	return r.GetCenterPoint().GetDistance(p)
}

func (r *Rect) Translate(delta Point) {
	r.p1.x += delta.x
	r.p1.y += delta.y
}
