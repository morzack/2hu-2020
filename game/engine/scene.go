package engine

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten"
)

const (
	StartSceneId = "START"
	StopSceneId  = "STOP"

	TileMapActorId    = "tilemapActor"
	PlayerActorId     = "player"
	OrderQueueActorId = "orderQueue"
	ScoreActorId      = "scoreActor"
	LevelTimerActorId = "levelTimerActor"

	SharedStateScoreKey = "score"
)

type SceneGenerator func(SceneManagerInterface) (SceneInterface, error)

var (
	ErrSceneNotPresent = fmt.Errorf("Scene Not Present")
)

type SceneManagerInterface interface {
	Start() error
	Stop() error
	Transition(nextSceneId string) error

	Update() error
	Draw(screen *ebiten.Image) error

	UpdateSceneId(id string, scene SceneGenerator)
	GetCurrentScene() SceneInterface
	GetRunning() bool

	UpdateSharedState(key, value string)
	GetSharedState(key string) (string, bool)
}

type SceneManager struct {
	currentScene string
	running      bool

	scenes          map[string]SceneGenerator
	currentSceneInt SceneInterface

	sharedState map[string]string
}

func NewSceneManager() (SceneManagerInterface, error) {
	sceneManager := SceneManager{
		scenes:      make(map[string]SceneGenerator, 0),
		sharedState: make(map[string]string, 0),
	}

	sceneManager.UpdateSceneId(StopSceneId, func(sceneMgr SceneManagerInterface) (SceneInterface, error) {
		stopBg, err := NewBlankBackground(color.Black)
		if err != nil {
			return nil, err
		}
		stopScene, err := NewScene(&sceneManager, StopSceneId, stopBg)
		if err != nil {
			return nil, err
		}
		return stopScene, nil
	})

	return &sceneManager, nil
}

func (s *SceneManager) Start() error {
	s.running = true
	return s.Transition(StartSceneId)
}

func (s *SceneManager) Stop() error {
	s.running = false
	return s.Transition(StopSceneId)
}

func (s *SceneManager) Transition(nextSceneId string) error {
	if currentScene := s.GetCurrentScene(); currentScene != nil {
		if err := currentScene.Stop(); err != nil {
			return err
		}
	}
	nextScene, present := s.scenes[nextSceneId]
	if !present {
		return ErrSceneNotPresent
	}
	s.currentScene = nextSceneId
	nextSceneInt, err := nextScene(s)
	if err != nil {
		return err
	}
	s.currentSceneInt = nextSceneInt
	return s.currentSceneInt.Start()
}

func (s *SceneManager) Update() error {
	if !s.running {
		return nil
	}
	if currentScene := s.GetCurrentScene(); currentScene != nil {
		return currentScene.Update()
	}
	return nil
}

func (s *SceneManager) Draw(screen *ebiten.Image) error {
	if !s.running {
		return nil
	}
	if currentScene := s.GetCurrentScene(); currentScene != nil {
		return currentScene.Draw(screen)
	}
	return nil
}

func (s *SceneManager) UpdateSceneId(id string, scene SceneGenerator) {
	s.scenes[id] = scene
}

func (s SceneManager) GetCurrentScene() SceneInterface {
	return s.currentSceneInt
}

func (s SceneManager) GetRunning() bool {
	return s.running
}

func (s *SceneManager) UpdateSharedState(key, value string) {
	s.sharedState[key] = value
}

func (s *SceneManager) GetSharedState(key string) (string, bool) {
	if val, present := s.sharedState[key]; present {
		return val, true
	}
	return "", false
}

type SceneInterface interface {
	Start() error
	Stop() error
	Update() error
	Draw(screen *ebiten.Image) error

	GetName() string

	RegisterActor(id string, actor ActorInterface)
	DeleteActor(id string)
	GetActor(id string) ActorInterface
	GetActorsType(actorType ActorType) []ActorInterface

	Transition(nextScene string) error
	UpdateSharedState(key, value string)
	GetSharedState(key string) (string, bool)
}

type Scene struct {
	name    string
	running bool

	actors map[string]ActorInterface

	background BackgroundInterface

	manager SceneManagerInterface
}

func NewScene(manager SceneManagerInterface, name string, background BackgroundInterface) (SceneInterface, error) {
	scene := Scene{
		name:    name,
		running: false,

		actors: make(map[string]ActorInterface, 0),

		manager: manager,
	}

	scene.background = background

	return &scene, nil
}

func (s *Scene) Start() error {
	s.running = true
	return nil
}

func (s *Scene) Stop() error {
	s.running = false
	return nil
}

func (s *Scene) Update() error {
	if s.running {
		for _, actor := range s.actors {
			if err := actor.Update(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Scene) Draw(screen *ebiten.Image) error {
	// render order (by actor type)
	// 1 (top):	orders (order queue)
	// 2:		ui
	// 3: 		items
	// 4:		player
	// 5:		tiles
	if s.running {
		s.background.Render(screen)
		for _, actor := range s.GetActorsType(ActorTypeTileMap) {
			if err := actor.Draw(screen); err != nil {
				return err
			}
		}
		for _, actor := range s.GetActorsType(ActorTypePlayer) {
			if err := actor.Draw(screen); err != nil {
				return err
			}
		}
		for _, actor := range s.GetActorsType(ActorTypeItem) {
			if err := actor.Draw(screen); err != nil {
				return err
			}
		}
		for _, actor := range s.GetActorsType(ActorTypeUIComponent) {
			if err := actor.Draw(screen); err != nil {
				return err
			}
		}
		for _, actor := range s.GetActorsType(ActorTypeOrderQueue) {
			if err := actor.Draw(screen); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s Scene) GetName() string {
	return s.name
}

func (s *Scene) RegisterActor(id string, actor ActorInterface) {
	s.actors[id] = actor
}

func (s *Scene) DeleteActor(id string) {
	delete(s.actors, id)
}

func (s *Scene) GetActor(id string) ActorInterface {
	return s.actors[id]
}

func (s *Scene) GetActorsType(actorType ActorType) []ActorInterface {
	actors := make([]ActorInterface, 0)
	for _, actor := range s.actors {
		if actor.GetType() == actorType {
			actors = append(actors, actor)
		}
	}
	return actors
}

func (s *Scene) Transition(nextScene string) error {
	return s.manager.Transition(nextScene)
}

func (s *Scene) UpdateSharedState(key, value string) {
	s.manager.UpdateSharedState(key, value)
}

func (s *Scene) GetSharedState(key string) (string, bool) {
	return s.manager.GetSharedState(key)
}
