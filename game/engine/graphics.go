package engine

import (
	"fmt"
	"image"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

const (
	ScreenWidth  = 600
	ScreenHeight = 500
)

func LoadImageFromPath(path string) (*ebiten.Image, error) {
	imgReader, err := ebitenutil.OpenFile(path)
	if err != nil {
		return nil, err
	}
	img, _, err := image.Decode(imgReader)
	if err != nil {
		return nil, err
	}
	ebitenImage, err := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
	if err != nil {
		return nil, err
	}
	return ebitenImage, nil
}

type SpriteInterface interface {
	Render(screen *ebiten.Image, x, y, w, h, angle float64) error
	GetSize() (float64, float64)
}

type Sprite struct {
	loadedImage *ebiten.Image

	imageWidth, imageHeight float64
}

func NewSpriteFromPath(path string) (SpriteInterface, error) {
	s := Sprite{}

	lImage, err := LoadImageFromPath(path)
	if err != nil {
		return nil, err
	}
	s.loadedImage = lImage

	wInt, hInt := s.loadedImage.Size()
	s.imageWidth = float64(wInt)
	s.imageHeight = float64(hInt)

	return s, nil
}

func (s Sprite) Render(screen *ebiten.Image, x, y, w, h, angle float64) error {
	drawOptions := ebiten.DrawImageOptions{}
	drawOptions.GeoM.Reset()
	drawOptions.GeoM.Scale(w/s.imageWidth, h/s.imageHeight)
	drawOptions.GeoM.Rotate(2 * math.Pi * angle)
	drawOptions.GeoM.Translate(x, y)
	return screen.DrawImage(s.loadedImage, &drawOptions)
}

func (s Sprite) GetSize() (float64, float64) {
	return s.imageWidth, s.imageHeight
}

type DummySprite struct {
}

func NewDummySprite() SpriteInterface {
	return &DummySprite{}
}

func (d DummySprite) Render(screen *ebiten.Image, x, y, w, h, angle float64) error {
	return nil
}

func (d DummySprite) GetSize() (float64, float64) {
	return 0, 0
}

type AnimatedSprite struct {
	loadedImages            []*ebiten.Image
	imageWidth, imageHeight float64
	frameCounter, period    int
	currentFrame            int
}

func NewAnimatedSprite(directory string, period, frames int) (SpriteInterface, error) {
	s := AnimatedSprite{
		period:       period,
		loadedImages: make([]*ebiten.Image, 0),
	}

	for frame := 0; frame < frames; frame++ {
		file := fmt.Sprintf("%s/%d.png", directory, frame)
		img, err := LoadImageFromPath(file)
		if err != nil {
			return nil, err
		}
		s.loadedImages = append(s.loadedImages, img)
		w, h := img.Size()
		s.imageWidth = float64(w)
		s.imageHeight = float64(h)
	}

	return &s, nil
}

func (a *AnimatedSprite) Render(screen *ebiten.Image, x, y, w, h, angle float64) error {
	a.frameCounter++
	if a.frameCounter%a.period == 0 {
		a.currentFrame++
	}
	if a.currentFrame >= len(a.loadedImages) {
		a.currentFrame = 0
	}

	drawOptions := ebiten.DrawImageOptions{}
	drawOptions.GeoM.Reset()
	drawOptions.GeoM.Scale(w/a.imageWidth, h/a.imageHeight)
	drawOptions.GeoM.Rotate(2 * math.Pi * angle)
	drawOptions.GeoM.Translate(x, y)
	return screen.DrawImage(a.loadedImages[a.currentFrame], &drawOptions)
}

func (a *AnimatedSprite) GetSize() (float64, float64) {
	return a.imageWidth, a.imageHeight
}

type BackgroundInterface interface {
	Render(screen *ebiten.Image) error
}

type Background struct {
	sprite SpriteInterface
}

func NewBackground(filename string) (BackgroundInterface, error) {
	bg := Background{}

	bgImg, err := NewSpriteFromPath(filename)
	if err != nil {
		return nil, err
	}
	bg.sprite = bgImg

	return &bg, nil
}

func (b Background) Render(screen *ebiten.Image) error {
	return b.sprite.Render(screen, 0, 0, ScreenWidth, ScreenHeight, 0)
}

type BlankBackground struct {
	color color.Color
}

func NewBlankBackground(color color.Color) (BackgroundInterface, error) {
	bg := BlankBackground{
		color: color,
	}
	return &bg, nil
}

func (b BlankBackground) Render(screen *ebiten.Image) error {
	return screen.Fill(b.color)
}
