package engine

import (
	"io/ioutil"
	"strings"

	"github.com/hajimehoshi/ebiten"
)

type TileType int

const (
	TileTypeAir       TileType = iota
	TileTypeCounter   TileType = iota
	TileTypeDispenser TileType = iota
	TileTypeGriddle   TileType = iota
	TileTypeGrill     TileType = iota
	TileTypeReceiver  TileType = iota
	TileTypeGarbage   TileType = iota

	dispenserSecondsRecharge = 5
)

type TileMapInterface interface {
	Draw(screen *ebiten.Image) error
	Update() error

	GetTileAt(x, y int) TileInterface
	GetTileAtScreenCoords(x, y float64) TileInterface

	GetBoundingBoxes() []Rect
	CollideWithTiles(collider Rect) bool

	GetPlayerStartPos() Point
}

type TileMap struct {
	Actor
	width, height         int
	tileWidth, tileHeight int
	tiles                 [][]TileInterface
	boundingBoxes         []Rect
	playerStart           Point
}

func NewTileMap(scene SceneInterface, mapPath string, w, h, tileWidth, tileHeight int) (TileMap, error) {
	tMap := TileMap{
		Actor{
			x:         0,
			y:         0,
			scene:     scene,
			name:      "tileMap",
			actorType: ActorTypeTileMap,
		},
		w, h,
		tileWidth, tileHeight,
		make([][]TileInterface, 0),
		make([]Rect, 0),
		Point{0, 0},
	}

	tiles := make([][]TileInterface, h)
	for i := range tiles {
		tiles[i] = make([]TileInterface, w)
		for j := range tiles[i] {
			t, _ := NewAirTile()
			tiles[i][j] = t
		}
	}

	dat, err := ioutil.ReadFile(mapPath)
	if err != nil {
		return TileMap{}, err
	}
	for i, v := range strings.Split(string(dat), "\r\n") {
		for j := range v {
			x := j * tileWidth
			y := i * tileHeight
			tileChar := string(v[j])
			if tileChar == "@" { // special case to show where to start player
				tMap.playerStart = Point{float64(x), float64(y)}
			}
			tile, err := NewTileFromCharacter(scene, tileChar,
				float64(x), float64(y), float64(tileWidth), float64(tileHeight))
			if err != nil {
				return TileMap{}, err
			}
			tiles[i][j] = tile
		}
	}

	tMap.tiles = tiles

	bBoxes := make([]Rect, 0)
	for rowIndex := range tiles {
		for colIndex := range tiles[rowIndex] {
			if !tiles[rowIndex][colIndex].GetPassable() {
				bBoxes = append(bBoxes, Rect{
					p1: Point{float64(colIndex * tileWidth), float64(rowIndex * tileHeight)},
					p2: Point{float64(tileWidth), float64(tileHeight)},
				})
			}
		}
		// NOTE this is a borked optimization, only fix/use if things get spicy
		// row := tiles[rowIndex]
		// bboxStart := 0
		// buildingBbox := false
		// for colIndex := range row {
		// 	if row[colIndex].GetPassable() {
		// 		if buildingBbox {
		// 			buildingBbox = false
		// 			bBoxes = append(bBoxes, Rect{
		// 				p1: Point{float64(bboxStart * tileWidth), float64(rowIndex * tileHeight)},
		// 				p2: Point{float64(colIndex*tileWidth - 1), float64((rowIndex + 1) * tileHeight)},
		// 			})
		// 		}
		// 	} else {
		// 		if !buildingBbox {
		// 			bboxStart = colIndex
		// 			buildingBbox = true
		// 		}
		// 	}
		// }
	}
	tMap.boundingBoxes = bBoxes

	return tMap, nil
}

func (t TileMap) Draw(screen *ebiten.Image) error {
	for i := range t.tiles {
		for j := range t.tiles[i] {
			sprite := t.tiles[i][j].GetSprite()
			if err := sprite.Render(
				screen,
				float64(j*t.tileWidth), float64(i*t.tileHeight),
				float64(t.tileWidth), float64(t.tileHeight), 0); err != nil {
				return err
			}
		}
	}
	return nil
}

func (t TileMap) Update() error {
	for i := range t.tiles {
		for j := range t.tiles[i] {
			if err := t.tiles[i][j].Update(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (t *TileMap) GetTileAt(x, y int) TileInterface {
	if x >= t.width || x < 0 || y >= t.height || y < 0 {
		return nil
	}
	return t.tiles[y][x]
}

func (t *TileMap) GetTileAtScreenCoords(x, y float64) TileInterface {
	tX := int(x / float64(t.tileWidth))
	tY := int(y / float64(t.tileWidth))
	return t.GetTileAt(tX, tY)
}

func (t TileMap) GetBoundingBoxes() []Rect {
	return t.boundingBoxes
}

func (t TileMap) CollideWithTiles(collider Rect) bool {
	for _, bb := range t.GetBoundingBoxes() {
		if bb.CollidesWithRect(collider) {
			return true
		}
	}
	return false
}

func (t TileMap) GetPlayerStartPos() Point {
	return t.playerStart
}

type TileInterface interface {
	GetSprite() SpriteInterface
	Update() error
	Interact() error
	GetPassable() bool
	GetBoundingBox() Rect

	SetPosition(x, y float64)
	SetSize(width, height float64)

	IsEmpty() bool
	GetHeldItem() ItemInterface
	HoldItem(item ItemInterface) (bool, error)
	TakeItem() ItemInterface
}

type Tile struct {
	x, y          float64
	width, height float64

	tileType TileType
	passable bool
	sprite   SpriteInterface

	itemHolding ItemInterface
}

func NewTileFromCharacter(scene SceneInterface, s string, x, y, width, height float64) (TileInterface, error) {
	var t TileInterface
	var err error
	switch s {
	case " ":
		t, err = NewAirTile()
	case "@": // this is a special case that only marks where the player starts
		t, err = NewAirTile()
	case "|":
		t, err = NewCounterTile(scene)
	case "-":
		t, err = NewCounterSideTile(scene)
	case "g":
		t, err = NewGriddleTile(scene)
	case "[":
		t, err = NewGrillTile(scene)
	case "o":
		t, err = NewDispenserTile(scene, NewOctopusItem)
	case "b":
		t, err = NewDispenserTile(scene, NewBatterItem)
	case "c":
		t, err = NewDispenserTile(scene, NewChickenItem)
	case "s":
		t, err = NewDispenserTile(scene, NewRawSobaItem)
	case "p":
		t, err = NewDispenserTile(scene, NewPlateItem)
	case "/":
		t, err = NewDispenserTile(scene, NewSkewerItem)
	case ">":
		t, err = NewOrderReceiverTile(scene)
	case ".":
		t, err = NewGarbageTile(scene)
	default:
		t, err = NewAirTile()
	}
	if err != nil {
		return nil, err
	}
	t.SetPosition(x, y)
	t.SetSize(width, height)
	return t, nil
}

func (t Tile) GetSprite() SpriteInterface {
	return t.sprite
}

func (t *Tile) Update() error {
	if t.itemHolding != nil {
		item := t.itemHolding
		holdingSize := item.GetBoundingBox().p2
		tileCenter := t.GetBoundingBox().GetCenterPoint()
		item.SetPosition(tileCenter.x-holdingSize.x/2, tileCenter.y-holdingSize.y/2)
	}
	return nil
}

func (t *Tile) Interact() error {
	return nil
}

func (t Tile) GetPassable() bool {
	return t.passable
}

func (t Tile) GetBoundingBox() Rect {
	return Rect{
		p1: Point{t.x, t.y},
		p2: Point{t.width, t.height},
	}
}

func (t *Tile) SetPosition(x, y float64) {
	t.x = x
	t.y = y
}

func (t *Tile) SetSize(width, height float64) {
	t.width = width
	t.height = height
}

func (t Tile) IsEmpty() bool {
	if t.passable {
		return false
	}
	if t.itemHolding == nil {
		return true
	}
	return false
}

func (t *Tile) GetHeldItem() ItemInterface {
	return t.itemHolding
}

func (t *Tile) HoldItem(item ItemInterface) (bool, error) {
	if !t.IsEmpty() {
		return false, nil
	}
	t.itemHolding = item
	return true, nil
}

func (t *Tile) TakeItem() ItemInterface {
	if t.IsEmpty() {
		return nil
	}
	item := t.itemHolding
	t.itemHolding = nil
	return item
}

type AirTile struct {
	Tile
}

func NewAirTile() (TileInterface, error) {
	return &AirTile{
		Tile{
			tileType: TileTypeAir,
			passable: true,
			sprite:   NewDummySprite(),
		},
	}, nil
}

type CounterTile struct {
	Tile
	scene SceneInterface
}

func NewCounterTile(scene SceneInterface) (TileInterface, error) {
	counterSprite, err := NewSpriteFromPath("./assets/tiles/counter-bottom.png")
	if err != nil {
		return nil, err
	}
	return &CounterTile{
		Tile{
			tileType: TileTypeCounter,
			passable: false,
			sprite:   counterSprite,
		},
		scene,
	}, nil
}

func NewCounterSideTile(scene SceneInterface) (TileInterface, error) {
	counterSprite, err := NewSpriteFromPath("./assets/tiles/counter-side.png")
	if err != nil {
		return nil, err
	}
	return &CounterTile{
		Tile{
			tileType: TileTypeCounter,
			passable: false,
			sprite:   counterSprite,
		},
		scene,
	}, nil
}

func (c *CounterTile) IsEmpty() bool {
	// if the item on the counter is a plate or skewer, we know that we can try crafting
	if c.itemHolding == nil {
		return true
	}
	switch c.itemHolding.GetItemType() {
	case ItemTypePlate, ItemTypeSkewer:
		return true
	default:
		return false
	}
}

func (c *CounterTile) HoldItem(item ItemInterface) (bool, error) {
	if !c.IsEmpty() {
		return false, nil
	}
	if c.itemHolding == nil {
		c.itemHolding = item
		return true, nil
	}
	var newItem ItemInterface
	var err error
	switch c.itemHolding.GetItemType() {
	case ItemTypePlate:
		if item.GetItemType() == ItemTypeSoba {
			newItem, err = NewPreparedSobaItem(c.scene)
		} else if item.GetItemType() == ItemTypeTakoyaki {
			newItem, err = NewPreparedTakoyakiItem(c.scene)
		} else {
			return false, nil
		}
	case ItemTypeSkewer:
		if item.GetItemType() == ItemTypeYakitori {
			newItem, err = NewPreparedYakitoriItem(c.scene)
		} else {
			return false, nil
		}
	default:
		return false, nil
	}
	if err != nil {
		return false, err
	}
	c.scene.DeleteActor(item.GetName())
	c.scene.DeleteActor(c.itemHolding.GetName())
	c.scene.RegisterActor(newItem.GetName(), newItem)
	c.itemHolding = newItem
	return true, nil
}

type DispenserTile struct {
	Tile
	ready                   bool
	readySprite, idleSprite SpriteInterface

	readyTimer      *Timer
	secondsRecharge float64

	scene         SceneInterface
	itemSpawnFunc ItemDispenseFunction
}

func NewDispenserTile(scene SceneInterface, genItem ItemDispenseFunction) (TileInterface, error) {
	idleSprite, err := NewSpriteFromPath("./assets/tiles/dispenser/dispenser.png")
	if err != nil {
		return nil, err
	}
	readySprite, err := NewSpriteFromPath("./assets/tiles/dispenser/dispenser-ready.png")
	if err != nil {
		return nil, err
	}

	dispTile := DispenserTile{
		Tile{
			tileType: TileTypeDispenser,
			passable: false,
			sprite:   NewDummySprite(),
		},
		false,
		readySprite, idleSprite,
		&Timer{},
		dispenserSecondsRecharge,
		scene,
		genItem,
	}

	dispTimer := NewTimer(scene, GetID("dispenserTimer"))
	dispTimer.SetTargetSeconds(dispenserSecondsRecharge)
	dispTimer.SetListener(func() error {
		dispTile.ready = true
		return nil
	})
	dispTimer.Randomize()
	dispTimer.Start()
	scene.RegisterActor(dispTimer.Actor.GetName(), &dispTimer)
	dispTile.readyTimer = &dispTimer

	return &dispTile, nil
}

func (d DispenserTile) GetSprite() SpriteInterface {
	if d.ready {
		return d.readySprite
	}
	return d.idleSprite
}

func (d *DispenserTile) Interact() error {
	if d.ready && d.IsEmpty() {
		// spit out an item (new actor)
		newItem, err := d.itemSpawnFunc(d.scene)
		if err != nil {
			return err
		}
		d.HoldItem(newItem)
		d.scene.RegisterActor(newItem.GetName(), newItem)
		d.ready = false
		d.readyTimer.Reset()
		d.readyTimer.Start()
	}
	return nil
}

type CookingState int

const (
	GriddleIdle       CookingState = iota
	GriddleRunning    CookingState = iota
	GriddleComplete   CookingState = iota
	GriddleOvercooked CookingState = iota
	GriddleFire       CookingState = iota

	GriddleCookingTime    = 10
	GriddleOvercookedTime = 15
	GriddleItemCap        = 2
)

type GriddleTile struct {
	Tile

	activeSprite     SpriteInterface
	inactiveSprite   SpriteInterface
	completeSprite   SpriteInterface
	overcookedSprite SpriteInterface
	fireSprite       SpriteInterface

	state CookingState

	cookingTimer    *Timer
	overcookedTimer *Timer
	progressBar     ProgressBarInterface
	scene           SceneInterface

	itemsHolding []ItemInterface
}

func NewGriddleTile(scene SceneInterface) (TileInterface, error) {
	spr, err := NewAnimatedSprite("./assets/tiles/griddle", 45, 4)
	if err != nil {
		return nil, err
	}

	griddleTile := GriddleTile{
		Tile{
			tileType: TileTypeGriddle,
			passable: false,
			sprite:   NewDummySprite(),
		},

		spr, spr, spr, spr, spr,

		GriddleIdle,

		&Timer{}, &Timer{},
		nil,
		scene,

		make([]ItemInterface, 0),
	}

	cookingTimer := NewTimer(scene, GetID("griddleCookingTimer"))
	cookingTimer.SetTargetSeconds(GriddleCookingTime)
	cookingTimer.SetListener(func() error {
		octopusPresent := false
		batterPresent := false
		for _, v := range griddleTile.itemsHolding {
			switch v.(*Item).GetItemType() {
			case ItemTypeOctopus:
				octopusPresent = true
			case ItemTypeBatter:
				batterPresent = true
			}
			griddleTile.scene.DeleteActor(v.GetName())
		}
		var holdingItem ItemInterface
		var err error
		if !(octopusPresent && batterPresent) {
			// recipe failed, don't move to overcooked phase
			holdingItem, err = NewOvercookedItem(griddleTile.scene)
			griddleTile.state = GriddleOvercooked
		} else {
			// recipe succeeded, move into overcooked phase
			holdingItem, err = NewTakoyakiItem(griddleTile.scene)
			griddleTile.state = GriddleComplete
			griddleTile.overcookedTimer.Reset()
			griddleTile.overcookedTimer.Start()
		}
		if err != nil {
			return err
		}
		griddleTile.scene.RegisterActor(holdingItem.GetName(), holdingItem)
		griddleTile.itemsHolding = []ItemInterface{holdingItem}
		griddleTile.cookingTimer.Reset()
		return nil
	})
	cookingTimer.Reset()
	scene.RegisterActor(cookingTimer.Actor.GetName(), &cookingTimer)
	griddleTile.cookingTimer = &cookingTimer

	overcookedTimer := NewTimer(scene, GetID("griddleOvercookedTimer"))
	overcookedTimer.SetTargetSeconds(GriddleOvercookedTime)
	overcookedTimer.SetListener(func() error {
		griddleTile.state = GriddleOvercooked
		for _, v := range griddleTile.itemsHolding {
			griddleTile.scene.DeleteActor(v.GetName())
		}
		overcookedItem, err := NewOvercookedItem(griddleTile.scene)
		if err != nil {
			return err
		}
		griddleTile.scene.RegisterActor(overcookedItem.GetName(), overcookedItem)
		griddleTile.itemsHolding = []ItemInterface{overcookedItem}
		griddleTile.overcookedTimer.Reset()
		return nil
	})
	overcookedTimer.Reset()
	scene.RegisterActor(overcookedTimer.Actor.GetName(), &overcookedTimer)
	griddleTile.overcookedTimer = &overcookedTimer

	progressBar, err := NewProgressBar(scene, func() float64 {
		if griddleTile.cookingTimer.active {
			return griddleTile.cookingTimer.GetPercentComplete()
		}
		if griddleTile.overcookedTimer.active {
			return griddleTile.overcookedTimer.GetPercentComplete()
		}
		return 0
	}, "./assets/progress-bar", 0, 0, 30, 10)
	if err != nil {
		return nil, err
	}
	scene.RegisterActor(progressBar.GetName(), progressBar)
	griddleTile.progressBar = progressBar

	return &griddleTile, nil
}

func (g GriddleTile) GetSprite() SpriteInterface {
	switch g.state {
	case GriddleIdle:
		return g.inactiveSprite
	case GriddleRunning:
		return g.activeSprite
	case GriddleComplete:
		return g.completeSprite
	case GriddleOvercooked:
		return g.overcookedSprite
	case GriddleFire:
		return g.fireSprite
	default:
		return g.inactiveSprite
	}
}

func (g GriddleTile) IsEmpty() bool {
	switch g.state {
	case GriddleIdle, GriddleRunning:
		return len(g.itemsHolding) < GriddleItemCap
	case GriddleComplete, GriddleOvercooked, GriddleFire:
		return false
	default:
		return false
	}
}

func (g GriddleTile) GetHeldItem() ItemInterface {
	// NOTE this doesn't really work that well with multiple items.
	// just ignore it
	return nil
}

func (g *GriddleTile) HoldItem(item ItemInterface) (bool, error) {
	if !g.IsEmpty() {
		return false, nil
	}
	switch g.state {
	case GriddleIdle:
		// take the item and start the griddle
		g.itemsHolding = append(g.itemsHolding, item)
		g.cookingTimer.Start()
		return true, nil
	case GriddleRunning:
		g.itemsHolding = append(g.itemsHolding, item)
		return true, nil
	}
	return false, nil
}

func (g *GriddleTile) TakeItem() ItemInterface {
	switch g.state {
	case GriddleIdle, GriddleRunning, GriddleFire:
		return nil
	case GriddleComplete, GriddleOvercooked:
		completedItem := g.itemsHolding[0]
		g.itemsHolding = make([]ItemInterface, 0)
		g.state = GriddleIdle
		g.overcookedTimer.Reset()
		return completedItem
	default:
		return nil
	}
}

func (g *GriddleTile) Update() error {
	// here we just want to set the position of all the stuff being cooked
	// NOTE hard coded to support two items for now.
	tileCenter := g.GetBoundingBox().GetCenterPoint()
	if len(g.itemsHolding) == 1 {
		item := g.itemsHolding[0]
		holdingSize := item.GetBoundingBox().p2
		item.SetPosition(tileCenter.x-holdingSize.x/2, tileCenter.y-holdingSize.y/2)
	}
	if len(g.itemsHolding) == 2 {
		item1 := g.itemsHolding[0]
		item1.SetSize(TinyItemWidth, TinyItemHeight)
		item2 := g.itemsHolding[1]
		item2.SetSize(TinyItemWidth, TinyItemHeight)
		holdingSize := item1.GetBoundingBox().p2 // safe assumption that they're the same size
		item1.SetPosition(tileCenter.x-2*holdingSize.x/2, tileCenter.y-holdingSize.y/2)
		item2.SetPosition(tileCenter.x, tileCenter.y-holdingSize.y/2)
	}
	g.progressBar.SetPosition(g.x+10, g.y+g.height)
	return nil
}

const (
	GrillIdle       CookingState = iota
	GrillRunning    CookingState = iota
	GrillComplete   CookingState = iota
	GrillOvercooked CookingState = iota
	GrillFire       CookingState = iota

	GrillCookingTime    = 10
	GrillOvercookedTime = 15
)

type GrillTile struct {
	Tile

	activeSprite     SpriteInterface
	inactiveSprite   SpriteInterface
	completeSprite   SpriteInterface
	overcookedSprite SpriteInterface
	fireSprite       SpriteInterface

	state CookingState

	cookingTimer    *Timer
	overcookedTimer *Timer
	progressBar     ProgressBarInterface
	scene           SceneInterface
}

func NewGrillTile(scene SceneInterface) (TileInterface, error) {
	spr, err := NewAnimatedSprite("./assets/tiles/grill", 45, 4)
	if err != nil {
		return nil, err
	}

	grillTile := GrillTile{
		Tile{
			tileType: TileTypeGrill,
			passable: false,
			sprite:   NewDummySprite(),
		},

		spr, spr, spr, spr, spr,

		GrillIdle,
		&Timer{}, &Timer{},
		nil,
		scene,
	}

	cookingTimer := NewTimer(scene, GetID("grillCookingTimer"))
	cookingTimer.SetTargetSeconds(GrillCookingTime)
	cookingTimer.SetListener(func() error {
		var newItem ItemInterface
		var err error
		switch grillTile.itemHolding.GetItemType() {
		case ItemTypeChicken:
			newItem, err = NewYakitoriItem(grillTile.scene)
			grillTile.state = GrillComplete
		case ItemTypeRawSoba:
			newItem, err = NewSobaItem(grillTile.scene)
			grillTile.state = GrillComplete
		default:
			newItem, err = NewOvercookedItem(grillTile.scene)
			grillTile.state = GrillOvercooked
		}
		if err != nil {
			return err
		}
		grillTile.scene.DeleteActor(grillTile.itemHolding.GetName())
		grillTile.scene.RegisterActor(newItem.GetName(), newItem)
		grillTile.itemHolding = newItem
		if grillTile.state == GrillComplete {
			grillTile.overcookedTimer.Reset()
			grillTile.overcookedTimer.Start()
		}
		grillTile.cookingTimer.Reset()
		return nil
	})
	cookingTimer.Reset()
	scene.RegisterActor(cookingTimer.Actor.GetName(), &cookingTimer)
	grillTile.cookingTimer = &cookingTimer

	overcookedTimer := NewTimer(scene, GetID("grillOvercookedTimer"))
	overcookedTimer.SetTargetSeconds(GrillOvercookedTime)
	overcookedTimer.SetListener(func() error {
		grillTile.state = GrillOvercooked
		grillTile.scene.DeleteActor(grillTile.itemHolding.GetName())
		overcookedItem, err := NewOvercookedItem(grillTile.scene)
		if err != nil {
			return err
		}
		grillTile.scene.RegisterActor(overcookedItem.GetName(), overcookedItem)
		grillTile.itemHolding = overcookedItem
		grillTile.overcookedTimer.Reset()
		return nil
	})
	overcookedTimer.Reset()
	scene.RegisterActor(overcookedTimer.Actor.GetName(), &overcookedTimer)
	grillTile.overcookedTimer = &overcookedTimer

	progressBar, err := NewProgressBar(scene, func() float64 {
		if grillTile.cookingTimer.active {
			return grillTile.cookingTimer.GetPercentComplete()
		}
		if grillTile.overcookedTimer.active {
			return grillTile.overcookedTimer.GetPercentComplete()
		}
		return 0
	}, "./assets/progress-bar", 0, 0, 30, 10)
	if err != nil {
		return nil, err
	}
	scene.RegisterActor(progressBar.GetName(), progressBar)
	grillTile.progressBar = progressBar

	return &grillTile, nil
}

func (g GrillTile) GetSprite() SpriteInterface {
	switch g.state {
	case GrillIdle:
		return g.inactiveSprite
	case GrillRunning:
		return g.activeSprite
	case GrillComplete:
		return g.completeSprite
	case GrillOvercooked:
		return g.overcookedSprite
	case GrillFire:
		return g.fireSprite
	default:
		return g.inactiveSprite
	}
}

func (g GrillTile) IsEmpty() bool {
	if g.state == GrillIdle && g.itemHolding == nil {
		return true
	}
	return false
}

func (g *GrillTile) HoldItem(item ItemInterface) (bool, error) {
	if !g.IsEmpty() {
		return false, nil
	}
	g.itemHolding = item
	g.cookingTimer.Start()
	return true, nil
}

func (g *GrillTile) TakeItem() ItemInterface {
	switch g.state {
	case GrillIdle, GrillRunning, GrillFire:
		return nil
	case GrillComplete, GrillOvercooked:
		completedItem := g.itemHolding
		g.itemHolding = nil
		g.state = GrillIdle
		g.overcookedTimer.Reset()
		return completedItem
	default:
		return nil
	}
}

func (g *GrillTile) Update() error {
	if g.itemHolding != nil {
		item := g.itemHolding
		holdingSize := item.GetBoundingBox().p2
		tileCenter := g.GetBoundingBox().GetCenterPoint()
		item.SetPosition(tileCenter.x-holdingSize.x/2, tileCenter.y-holdingSize.y/2)
	}
	g.progressBar.SetPosition(g.x+10, g.y+g.height)
	return nil
}

type OrderReceiverTile struct {
	Tile
	scene SceneInterface
}

func NewOrderReceiverTile(scene SceneInterface) (TileInterface, error) {
	receiverSprite, err := NewAnimatedSprite("./assets/tiles/receiver", 40, 4)
	if err != nil {
		return nil, err
	}
	return &OrderReceiverTile{
		Tile{
			tileType: TileTypeReceiver,
			passable: false,
			sprite:   receiverSprite,
		},
		scene,
	}, nil
}

func (o *OrderReceiverTile) IsEmpty() bool {
	return true
}

func (o *OrderReceiverTile) HoldItem(item ItemInterface) (bool, error) {
	var success bool
	var err error
	switch item.GetItemType() {
	case ItemTypePreparedSoba:
		o.scene.DeleteActor(item.GetName())
		success, err = o.scene.GetActor(OrderQueueActorId).(*OrderQueue).
			CompleteOrder(OrderTypeSoba)
	case ItemTypePreparedTakoyaki:
		o.scene.DeleteActor(item.GetName())
		success, err = o.scene.GetActor(OrderQueueActorId).(*OrderQueue).
			CompleteOrder(OrderTypeTakoyaki)
	case ItemTypePreparedYakitori:
		o.scene.DeleteActor(item.GetName())
		success, err = o.scene.GetActor(OrderQueueActorId).(*OrderQueue).
			CompleteOrder(OrderTypeYakitori)
	default:
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if success {
		switch item.GetItemType() {
		case ItemTypePreparedSoba, ItemTypePreparedYakitori:
			o.scene.GetActor(ScoreActorId).(*Score).
				AddScore(5)
		case ItemTypePreparedTakoyaki:
			o.scene.GetActor(ScoreActorId).(*Score).
				AddScore(10)
		}
	} else {
		switch item.GetItemType() {
		case ItemTypePreparedSoba, ItemTypePreparedYakitori:
			o.scene.GetActor(ScoreActorId).(*Score).
				AddScore(2)
		case ItemTypePreparedTakoyaki:
			o.scene.GetActor(ScoreActorId).(*Score).
				AddScore(3)
		}
	}
	return true, nil
}

type GarbageTile struct {
	Tile
	scene SceneInterface
}

func NewGarbageTile(scene SceneInterface) (TileInterface, error) {
	garbageSprite, err := NewSpriteFromPath("./assets/tiles/garbage.png")
	if err != nil {
		return nil, err
	}
	return &GarbageTile{
		Tile{
			tileType: TileTypeGarbage,
			passable: false,
			sprite:   garbageSprite,
		},
		scene,
	}, nil
}

func (g GarbageTile) IsEmpty() bool {
	return true
}

func (g *GarbageTile) HoldItem(item ItemInterface) (bool, error) {
	g.scene.DeleteActor(item.GetName())
	return true, nil
}
