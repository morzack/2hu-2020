package engine

import (
	"fmt"
	"math/rand"
)

type TimerListener func() error

type Timer struct {
	Actor
	currentTime, targetTime int
	active                  bool
	listener                TimerListener
}

func NewTimer(scene SceneInterface, name string) Timer {
	return Timer{
		Actor{
			x:         0,
			y:         0,
			name:      name,
			scene:     scene,
			actorType: ActorTypeTimer,
		},
		0, 0, false,
		func() error { return nil },
	}
}

func (t *Timer) SetListener(fn TimerListener) {
	t.listener = fn
}

func (t *Timer) Reset() {
	t.currentTime = 0
	t.active = false
}

func (t *Timer) SetTargetSeconds(seconds float64) {
	frames := int(seconds * 60)
	t.targetTime = frames
}

func (t *Timer) Start() {
	t.active = true
}

func (t *Timer) Stop() {
	t.Reset()
}

func (t *Timer) Pause() {
	t.active = false
}

func (t *Timer) Randomize() {
	t.currentTime = int(rand.Float64() * float64(t.targetTime))
}

func (t Timer) GetPercentComplete() float64 {
	return float64(t.currentTime) / float64(t.targetTime)
}

func (t Timer) GetSecondsRemaining() float64 {
	return float64(t.targetTime-t.currentTime) / 60
}

func (t Timer) GetTargetTime() float64 {
	return float64(t.targetTime) / 60
}

func (t Timer) GetSecondsPassed() float64 {
	return float64(t.currentTime) / 60
}

func (t *Timer) Update() error {
	if t.active {
		t.currentTime++
		if t.currentTime > t.targetTime {
			t.Reset()
			if err := t.listener(); err != nil {
				return err
			}
		}
	}
	return nil
}

func GetID(basename string) string {
	return fmt.Sprintf("%s%d", basename, rand.Intn(100000))
}
