package engine

import "github.com/hajimehoshi/ebiten"

type ActorType int

const (
	ActorTypeItem        ActorType = iota
	ActorTypeTimer       ActorType = iota
	ActorTypeTileMap     ActorType = iota
	ActorTypeUIComponent ActorType = iota
	ActorTypePlayer      ActorType = iota
	ActorTypeOrder       ActorType = iota
	ActorTypeOrderQueue  ActorType = iota
)

type ActorInterface interface {
	Update() error
	Draw(screen *ebiten.Image) error

	GetPosition() (x, y float64)
	SetPosition(x, y float64)
	GetName() string
	GetType() ActorType

	GetScene() SceneInterface
}

type Actor struct {
	x, y      float64
	name      string
	actorType ActorType

	scene SceneInterface
}

func (a Actor) Update() error {
	return nil
}

func (a Actor) Draw(screen *ebiten.Image) error {
	return nil
}

func (a Actor) GetPosition() (x, y float64) {
	return a.x, a.y
}

func (a *Actor) SetPosition(x, y float64) {
	a.x = x
	a.y = y
}

func (a Actor) GetName() string {
	return a.name
}

func (a Actor) GetType() ActorType {
	return a.actorType
}

func (a Actor) GetScene() SceneInterface {
	return a.scene
}
