package engine

import (
	"math"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
)

const (
	PlayerWidth  = 66
	PlayerHeight = 72

	PlayerSpeed = 0.05

	PlayerReachDistance = 120
)

type PlayerInterface interface {
	ActorInterface

	GetBoundingBox() Rect
	GetCurrentSprite() SpriteInterface

	Walk(horiz, vertical float64)

	InteractMap() error
	InteractItem() error

	InventoryEmpty() bool
	PickupItemTile(tile TileInterface) bool
}

type Player struct {
	Actor

	holding ItemInterface

	spriteIdle SpriteInterface
	spriteWalk SpriteInterface

	dx, dy  float64
	walking bool
}

func NewPlayer(scene SceneInterface, startPos Point) (Player, error) {
	spriteIdle, err := NewSpriteFromPath("./assets/player/mystia.png")
	if err != nil {
		return Player{}, err
	}
	spriteWalk, err := NewAnimatedSprite("./assets/player/mystia-walk", 20, 4)
	if err != nil {
		return Player{}, err
	}

	p := Player{
		Actor{
			x:         startPos.x,
			y:         startPos.y,
			name:      PlayerActorId,
			actorType: ActorTypePlayer,
			scene:     scene,
		},
		nil,
		spriteIdle, spriteWalk,
		0, 0,
		false,
	}

	return p, nil
}

func (p *Player) Update() error {
	// move the player in the direction of the mouse cursor
	cursorX, cursorY := ebiten.CursorPosition()
	playerCenter := p.GetBoundingBox().GetCenterPoint()
	p.Walk(float64(cursorX)-playerCenter.x, float64(cursorY)-playerCenter.y)

	p.walking = false
	if math.Hypot(p.dx, p.dy) > 3 {
		p.walking = true
	}

	// take 1px steps in the walk direction until we can't anymore
	canWalk := true
	for canWalk {
		walked := false

		// try to move horiz
		horizBB := p.GetBoundingBox()
		dx := 0
		if p.dx >= 1 {
			dx = 1
			horizBB.Translate(Point{float64(dx), 0})
			p.dx--
		}
		if p.dx <= -1 {
			dx = -1
			horizBB.Translate(Point{float64(dx), 0})
			p.dx++
		}
		if !p.Actor.scene.GetActor(TileMapActorId).(*TileMap).CollideWithTiles(horizBB) {
			p.x += float64(dx)
			walked = true
		}

		// move vert
		vertBB := p.GetBoundingBox()
		dy := 0
		if p.dy >= 1 {
			dy = 1
			vertBB.Translate(Point{0, float64(dy)})
			p.dy--
		}
		if p.dy <= -1 {
			dy = -1
			vertBB.Translate(Point{0, float64(dy)})
			p.dy++
		}
		if !p.Actor.scene.GetActor(TileMapActorId).(*TileMap).CollideWithTiles(vertBB) {
			p.y += float64(dy)
			walked = true
		}

		if !walked || dy == 0 && dx == 0 {
			canWalk = false
		}
	}
	if err := p.InteractMap(); err != nil {
		return err
	}
	if err := p.InteractItem(); err != nil {
		return err
	}
	if !p.InventoryEmpty() {
		holdingSize := p.holding.GetBoundingBox().p2
		p.holding.SetPosition(playerCenter.x-holdingSize.x/2, playerCenter.y-holdingSize.y/2)
	}
	return nil
}

func (p *Player) Draw(screen *ebiten.Image) error {
	return p.GetCurrentSprite().
		Render(screen, p.x, p.y, PlayerWidth, PlayerHeight, 0)
}

func (p Player) GetBoundingBox() Rect {
	return Rect{
		p1: Point{p.x, p.y},
		p2: Point{PlayerWidth, PlayerHeight},
	}
}

func (p *Player) GetCurrentSprite() SpriteInterface {
	if p.walking {
		return p.spriteWalk
	}
	return p.spriteIdle
}

func (p *Player) Walk(horiz, vertical float64) {
	p.dx = horiz * PlayerSpeed
	p.dy = vertical * PlayerSpeed
}

func (p *Player) InteractMap() error {
	// used for picking up items and triggering tiles
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		mouseX, mouseY := ebiten.CursorPosition()
		tileClicked := p.Actor.scene.GetActor(TileMapActorId).(*TileMap).
			GetTileAtScreenCoords(float64(mouseX), float64(mouseY))
		if tileClicked == nil {
			return nil
		}
		if tileClicked.GetBoundingBox().GetCenterDistance(p.GetBoundingBox().GetCenterPoint()) < PlayerReachDistance {
			if p.PickupItem(tileClicked) {
				return nil
			}
			if tileClicked.IsEmpty() {
				return tileClicked.Interact()
			}
			return nil
		}
	}
	return nil
}

func (p *Player) InteractItem() error {
	// used for placing items
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonRight) {
		mouseX, mouseY := ebiten.CursorPosition()
		tileClicked := p.Actor.scene.GetActor(TileMapActorId).(*TileMap).
			GetTileAtScreenCoords(float64(mouseX), float64(mouseY))
		if tileClicked == nil {
			return nil
		}
		if tileClicked.GetBoundingBox().GetCenterDistance(p.GetBoundingBox().GetCenterPoint()) < PlayerReachDistance {
			if tileClicked.IsEmpty() && !p.InventoryEmpty() {
				if success, err := tileClicked.HoldItem(p.holding); success && err == nil {
					p.holding = nil
				} else if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (p Player) InventoryEmpty() bool {
	if p.holding == nil {
		return true
	}
	return false
}

func (p *Player) PickupItem(tile TileInterface) bool {
	if !p.InventoryEmpty() {
		return false
	}
	if item := tile.TakeItem(); item != nil {
		p.holding = item
		return true
	}
	return false
}
