package engine

import (
	"fmt"
	"image/color"
	"strconv"

	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/examples/resources/fonts"
	"github.com/hajimehoshi/ebiten/inpututil"
	"github.com/hajimehoshi/ebiten/text"
)

type MouseState int

type ClickHandler func() error

const (
	MouseNotPresent MouseState = iota
	MouseHover      MouseState = iota
	MouseClick      MouseState = iota
	MousePress      MouseState = iota
	MouseRelease    MouseState = iota

	ButtonFileIdle  = "idle.png"
	ButtonFileHover = "hover.png"
	ButtonFileClick = "click.png"
)

type ClickzoneInterface interface {
	CheckMouseInBox() bool
	UpdateState()
	GetState() MouseState
}

type RectClickzone struct {
	x, y          float64
	width, height float64
	currentState  MouseState
	buttonType    ebiten.MouseButton
}

func NewRectClickzone(x, y float64, width, height float64) RectClickzone {
	cz := RectClickzone{
		x: x,
		y: y,

		width:  width,
		height: height,
	}

	return cz
}

func (r RectClickzone) CheckMouseInBox() bool {
	mXRaw, mYRaw := ebiten.CursorPosition()
	mX := float64(mXRaw)
	mY := float64(mYRaw)
	if mX >= r.x && mX <= r.x+r.width &&
		mY >= r.y && mY <= r.y+r.height {
		return true
	}
	return false
}

func (r *RectClickzone) UpdateState() {
	if !r.CheckMouseInBox() {
		if r.currentState == MouseClick ||
			r.currentState == MousePress {
			r.currentState = MouseRelease
			return
		}
		r.currentState = MouseNotPresent
		return
	}

	if inpututil.IsMouseButtonJustPressed(r.buttonType) {
		r.currentState = MouseClick
		return
	} else if inpututil.IsMouseButtonJustReleased(r.buttonType) {
		if r.currentState == MouseClick ||
			r.currentState == MousePress {
			r.currentState = MouseRelease
		}
		return
	} else if r.currentState == MouseClick ||
		r.currentState == MousePress {
		r.currentState = MousePress
		return
	}

	r.currentState = MouseHover
}

func (r RectClickzone) GetState() MouseState {
	return r.currentState
}

type UIComponentInterface interface {
	ActorInterface
}

type Button struct {
	Actor

	x, y          float64
	width, height float64

	clickZone RectClickzone

	sprite   SpriteInterface
	pressed  SpriteInterface
	selected SpriteInterface

	onClick ClickHandler
}

func NewButton(scene SceneInterface, x, y, width, height float64, name string, sprite, pressed, selected SpriteInterface) (Button, error) {
	bt := Button{
		Actor{
			x:    x,
			y:    y,
			name: name,

			scene:     scene,
			actorType: ActorTypeUIComponent,
		},
		x,
		y,
		width,
		height,
		NewRectClickzone(x, y, width, height),
		sprite,
		pressed,
		selected,
		func() error { return nil },
	}

	return bt, nil
}

func NewButtonStaticSprites(scene SceneInterface, x, y, width, height float64, name string, spriteDir string) (Button, error) {
	idle, hover, click, err := LoadButtonStaticSpritesDir(spriteDir)
	if err != nil {
		return Button{}, err
	}
	return NewButton(scene, x, y, width, height, name, idle, click, hover)
}

func (b *Button) Update() error {
	b.clickZone.UpdateState()
	if b.clickZone.GetState() == MouseClick {
		if err := b.onClick(); err != nil {
			return err
		}
	}
	return nil
}

func (b Button) GetSprite() SpriteInterface {
	switch b.clickZone.GetState() {
	case MouseNotPresent:
		return b.sprite
	case MouseHover:
		return b.selected
	case MouseClick:
		return b.pressed
	case MousePress:
		return b.pressed
	case MouseRelease:
		return b.pressed
	default:
		return b.sprite
	}
}

func (b *Button) Draw(screen *ebiten.Image) error {
	return b.GetSprite().Render(screen, b.x, b.y, b.width, b.height, 0)
}

func (b *Button) RegisterOnClick(fn ClickHandler) {
	b.onClick = fn
}

func LoadButtonStaticSpritesDir(directory string) (bIdle, bHover, bClick SpriteInterface, err error) {
	bIdle, err = NewSpriteFromPath(fmt.Sprintf("%s/%s", directory, ButtonFileIdle))
	if err != nil {
		return
	}
	bHover, err = NewSpriteFromPath(fmt.Sprintf("%s/%s", directory, ButtonFileHover))
	if err != nil {
		return
	}
	bClick, err = NewSpriteFromPath(fmt.Sprintf("%s/%s", directory, ButtonFileClick))
	if err != nil {
		return
	}
	return
}

const (
	ScoreDPI  = 72
	ScoreSize = 30
)

var (
	ScoreTextColor = color.White
)

type ScoreInterface interface {
	UIComponentInterface

	GetScore() int
	AddScore(i int)
	RemoveScore(i int)
}

type Score struct {
	Actor
	scoreFont font.Face
}

func NewScore(scene SceneInterface, x, y float64, resetScore bool) (UIComponentInterface, error) {
	tt, err := truetype.Parse(fonts.ArcadeN_ttf)
	if err != nil {
		return nil, err
	}

	fontNormal := truetype.NewFace(tt, &truetype.Options{
		Size:    ScoreSize,
		DPI:     ScoreDPI,
		Hinting: font.HintingFull,
	})

	score := Score{
		Actor{
			x:    x,
			y:    y,
			name: ScoreActorId,

			scene:     scene,
			actorType: ActorTypeUIComponent,
		},
		fontNormal,
	}

	if resetScore {
		scene.UpdateSharedState(SharedStateScoreKey, "0")
	}

	return &score, nil
}

func (s Score) GetScore() int {
	rScore, _ := s.scene.GetSharedState(SharedStateScoreKey)
	iScore, err := strconv.Atoi(rScore)
	if err != nil {
		return 0
	}
	return iScore
}

func (s *Score) AddScore(i int) {
	if i < 0 {
		return
	}
	curScore := s.GetScore()
	s.scene.UpdateSharedState(SharedStateScoreKey, fmt.Sprintf("%d", curScore+i))
}

func (s *Score) RemoveScore(i int) {
	if i < 0 {
		return
	}
	curScore := s.GetScore()
	s.scene.UpdateSharedState(SharedStateScoreKey, fmt.Sprintf("%d", curScore-i))
}

func (s Score) Draw(screen *ebiten.Image) error {
	msg := fmt.Sprintf("$%d", s.GetScore())
	text.Draw(screen, msg, s.scoreFont, int(s.x), int(s.y), ScoreTextColor)
	return nil
}

const (
	TimerDPI  = 72
	TimerSize = 30
)

var (
	TimerTextColor = color.White
)

type LevelTimerInterface interface {
	UIComponentInterface
	GetMinutesRemaining() int
	GetSecondsRemaining() int
}

type LevelTimer struct {
	Actor
	timer     *Timer
	timerFont font.Face
}

func NewLevelTimer(scene SceneInterface, x, y, levelDuration float64) (UIComponentInterface, error) {
	tt, err := truetype.Parse(fonts.ArcadeN_ttf)
	if err != nil {
		return nil, err
	}

	fontNormal := truetype.NewFace(tt, &truetype.Options{
		Size:    TimerSize,
		DPI:     TimerDPI,
		Hinting: font.HintingFull,
	})

	levelTimer := LevelTimer{
		Actor{
			x:    x,
			y:    y,
			name: LevelTimerActorId,

			scene:     scene,
			actorType: ActorTypeUIComponent,
		},
		&Timer{},
		fontNormal,
	}

	levelTimerTimer := NewTimer(scene, GetID("levelTimerTimer"))
	levelTimerTimer.SetTargetSeconds(levelDuration)
	levelTimerTimer.SetListener(func() error {
		levelTimer.scene.UpdateSharedState(
			SharedStateScoreKey,
			fmt.Sprintf("%d", levelTimer.scene.GetActor(ScoreActorId).(*Score).GetScore()))
		levelTimer.scene.Transition("levelScore")
		return nil
	})
	levelTimerTimer.Reset()
	levelTimerTimer.Start()
	scene.RegisterActor(levelTimerTimer.GetName(), &levelTimerTimer)
	levelTimer.timer = &levelTimerTimer

	return &levelTimer, nil
}

func (l LevelTimer) GetMinutesRemaining() int {
	return int(l.timer.GetSecondsRemaining() / 60)
}

func (l LevelTimer) GetSecondsRemaining() int {
	return int(l.timer.GetSecondsRemaining()) % 60
}

func (l LevelTimer) Draw(screen *ebiten.Image) error {
	msg := fmt.Sprintf("%01d:%02d", l.GetMinutesRemaining(), l.GetSecondsRemaining())
	text.Draw(screen, msg, l.timerFont, int(l.x), int(l.y), TimerTextColor)
	return nil
}

type GetProgressPercent func() float64

type ProgressBarInterface interface {
	UIComponentInterface
	GetSprite() SpriteInterface
}

type ProgressBar struct {
	Actor
	getProgressFn GetProgressPercent

	width, height float64

	bar0Percent   SpriteInterface
	bar20Percent  SpriteInterface
	bar40Percent  SpriteInterface
	bar60Percent  SpriteInterface
	bar80Percent  SpriteInterface
	bar100Percent SpriteInterface
}

func NewProgressBar(scene SceneInterface, getProgressFn GetProgressPercent, barDir string, x, y, width, height float64) (ProgressBarInterface, error) {
	barZero, err := NewSpriteFromPath(fmt.Sprintf("%s/0.png", barDir))
	if err != nil {
		return nil, err
	}
	barTwenty, err := NewSpriteFromPath(fmt.Sprintf("%s/20.png", barDir))
	if err != nil {
		return nil, err
	}
	barFourty, err := NewSpriteFromPath(fmt.Sprintf("%s/40.png", barDir))
	if err != nil {
		return nil, err
	}
	barSixty, err := NewSpriteFromPath(fmt.Sprintf("%s/60.png", barDir))
	if err != nil {
		return nil, err
	}
	barEighty, err := NewSpriteFromPath(fmt.Sprintf("%s/80.png", barDir))
	if err != nil {
		return nil, err
	}
	barHundred, err := NewSpriteFromPath(fmt.Sprintf("%s/100.png", barDir))
	if err != nil {
		return nil, err
	}

	progressBar := ProgressBar{
		Actor{
			x:    x,
			y:    y,
			name: GetID("progressBar"),

			scene:     scene,
			actorType: ActorTypeUIComponent,
		},
		getProgressFn,
		width, height,
		barZero,
		barTwenty,
		barFourty,
		barSixty,
		barEighty,
		barHundred,
	}

	return &progressBar, nil
}

func (p *ProgressBar) GetSprite() SpriteInterface {
	curPercent := int((p.getProgressFn() * 100) / 20)
	switch curPercent {
	case 0:
		return p.bar0Percent
	case 1:
		return p.bar20Percent
	case 2:
		return p.bar40Percent
	case 3:
		return p.bar60Percent
	case 4:
		return p.bar80Percent
	case 5:
		return p.bar100Percent
	default:
		return p.bar0Percent
	}
}

func (p *ProgressBar) Draw(screen *ebiten.Image) error {
	sprite := p.GetSprite()
	return sprite.Render(screen, p.x, p.y, p.width, p.height, 0)
}
