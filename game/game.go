package game

import (
	"os"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/morzack/2hu-2020/game/engine"
	"gitlab.com/morzack/2hu-2020/game/scenes"
)

type Game struct {
	sceneManager engine.SceneManagerInterface
}

func NewGameInstance() (*Game, error) {
	game := Game{}

	sceneManager, err := engine.NewSceneManager()
	if err != nil {
		return nil, err
	}
	game.sceneManager = sceneManager

	game.sceneManager.UpdateSceneId(engine.StartSceneId, scenes.NewStartScene)
	game.sceneManager.UpdateSceneId("levelPlayer", scenes.NewLevelScene)
	game.sceneManager.UpdateSceneId("levelScore", scenes.NewScoreScene)

	ebiten.SetWindowSize(912, 760)
	ebiten.SetWindowTitle("Mystia's Food Festival Madness")

	if err := game.sceneManager.Start(); err != nil {
		return nil, err
	}

	return &game, nil
}

func (g *Game) Update(screen *ebiten.Image) error {
	// update loop
	if g.sceneManager.GetCurrentScene().GetName() == engine.StopSceneId {
		os.Exit(0)
	}
	return g.sceneManager.Update()
}

func (g *Game) Draw(screen *ebiten.Image) {
	// render loop
	if err := g.sceneManager.Draw(screen); err != nil {
		panic(err)
	}
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return engine.ScreenWidth, engine.ScreenHeight
}
